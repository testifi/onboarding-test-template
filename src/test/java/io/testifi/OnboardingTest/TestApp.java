package io.testifi.OnboardingTest;

import io.testifi.cast.testng.DefaultTestRunner;

public class TestApp {

    public static void main(String[] args) throws Exception {
        System.setProperty("testEnvironment", "localRun");
        System.setProperty("executionName", "localTestNG");
        DefaultTestRunner.runTestNg("OnboardingTest:debug");
    }

}

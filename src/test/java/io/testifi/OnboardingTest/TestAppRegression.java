package io.testifi.OnboardingTest;

import io.testifi.cast.testng.DefaultTestRunner;

public class TestAppRegression {

    public static void main(String[] args) throws Exception {
        System.setProperty("testEnvironment", "localRun");
        System.setProperty("executionName", "localTestNG");
        System.setProperty("selectedDevice", "remoteChrome");
        DefaultTestRunner.runTestNg("OnboardingTest:regression");
    }

}
